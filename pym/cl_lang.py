#-*- coding: utf-8 -*-

# Copyright 2008-2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os, gettext
from cl_overriding import __findFileMO

class GlobalParam(type):
    """ Метакласс для глобальных параметров
    """
    def __init__(cls, *args):
        cls.GP = []
        cls.GP.append("")

gettext.find = __findFileMO

class lang:
    """Класс многоязыковой поддержки lang для перевода сообщений программ на
другие языки.

Типичное использование:
    import sys
    import lang

    # язык сообщений английский
    tr = lang.lang(en)
    # язык определяется системой
    #tr = lang.lang()

    #Установка домена переводаldap
    # в последующем можно не использовать - задается глобально
    tr.setGlobalDomain('calc')
    # задается локально для одного файла
    #tr.setLocalDomain('calc')

    # Установка метода перевода для текущего модуля
    tr.setLanguage(sys.modules[__name__])

Где:
    tr -- объект перевода
    'en' -- язык выводимых сообщений
    sys.modules[__name__] -- модуль сообщения которого переводятся
    calc - домен перевода - имя файла перевода без расширения

Если файл перевода не найден то сообщения не переводятся
Если в модуле сообщения которого переводим, экспортируются другие модули то
они тоже переводятся.

По умолчанию директория в которой находятся переводы: 'lang/i18h' относительно
исполняемого файла названия файлов перевода совпадают с названиями модулей
если не определен методом setDomainTranslate() - домен перевода.
    """
    __metaclass__ = GlobalParam
    def __init__(self,l=''):
        self.nameDomain = self.GP[0]
        #self.nameDomain = ''
        """ Название файла перевода (Домен) если используется 1 файл перевода
        """
        self.__catalog = os.path.abspath('/usr/share/calculate/i18n')
        """ Путь к каталогу переводов (в этом каталоге
            ru_RU/LC_MESSAGES в котором файл перевода)
        """
        env = os.environ
        if l == "" and env.has_key('LANG'):
            l = env['LANG'].split('.')[0].split("_")[0]
        """ Определение языка """
        self.__modnames = {}
        """ Словарь переведенных модулей
            ключ --- имя модуля
            значение --- был ли модуль переведен (1 или 0)
        """
        self.__l = l
        """Язык перевода для всех модулей"""

    def __translate(self,message):
        """Метод translate возвращает полученное значение без
изменений"""
        return message

    def setLanguage(self,module):
        """ Установка языка перевода для модуля module.

            параметр --- экспортируемый модуль python
            если в этом модуле экспортируются другие модули
            то язык устанавливается и для них
            Метод запускается после экспорта модуля который будем переводить
        """
        t = vars(module)
        for i in dir(module):
            q = str(t[i])
            if 'module' in q and not '__' in i and not '/usr/lib' in q\
            and not 'built-in' in q :
                mod = vars(module)[i]
                self.__setLang(mod)
        return self.__setLang(module)

    def __setLang(self,module):
        """ Установка языка перевода для модуля module.

            В случае нахождения файла перевода возвращает истину.
            Во всех случаях устанавливает метод перевода для модуля.
            Если нет файла перевода метод перевода возвращает то же
            значение что получает
        """
        if module.__name__ in self.__modnames.keys():
            return True

        if self.nameDomain == '':
            if module.__name__ == "__main__":
                nameDomain =  module.__file__.split('.')[0]
            else:
                nameDomain =  module.__name__
        else:
            nameDomain = self.nameDomain

        if self.__l == 'en':
            module._ = self.__translate
            ret = 1
        else:
            la = []
            la.append(self.__l)
            if gettext.find(nameDomain,self.__catalog,la):
                """Если найден словарь то инициализируем переводчик"""
                transl = gettext.translation(nameDomain\
                                            ,self.__catalog,la)

                #module._ = transl.ugettext
                module._ = transl.gettext
                ret = 1
            else:
                module._ = self.__translate
                ret = 0
        self.__modnames[module.__name__] = ret
        return ret

    def getTranslatorByName(self,namemodule):
        """ Метод который по имени модуля определяет, был ли модуль с этим
        именем переведен
        """
        if self.__modnames.has_key(namemodule):
            return self.__modnames[namemodule]
        return 0

    def setGlobalDomain(self, nameDomain):
        """ Метод для установки домена перевода (глобально для всех файлов)
        """
        self.GP[0] = nameDomain
        self.nameDomain = self.GP[0]
        return True

    def setLocalDomain(self, nameDomain):
        """ Метод для установки домена перевода (локально для одного файла)
        """
        self.nameDomain = nameDomain
        return True
 
