#-*- coding: utf-8 -*-

# Copyright 2008-2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Allowed keys
#   mode -     variable mode: 'w' - you can change value from command line,
#              'r' you cann't change value from command line
#   type     - depricated
#   value    - value 
#   hide - show or not variable for display values

class Data:
    # computer hostname
    os_net_hostname = {'hide':True, 'mode':"w"}

    # allowed networks
    os_net_allow ={'hide':True}

    # ip for all network interfaces (comma delimeter)
    os_net_ip ={'hide':True}

    # network interfaces
    os_net_interfaces={'hide':True}

    # computer domain
    os_net_domain = {'hide':True, 'mode':"w"}

    # short system name (CLD)
    os_linux_shortname={'hide':True}

    # aliases and path to ini files
    cl_env_data = {'hide':True,
                   'value':[('default', '/etc/calculate/calculate2.env'),
                            ('local', '/var/calculate/calculate2.env'),
                            ('remote', '/var/calculate/remote/calculate2.env')]}

    # path aliases to ini files (from cl_env_data)
    cl_env_location = {'hide':True}

    # path to ini files (from cl_env_data)
    cl_env_path = {'hide':True}

    # path to information file on server
    cl_env_server_path = {'hide':True}

    # paths to template files
    cl_template_path = {'hide':True}

    # paths to clt-template files
    cl_template_clt_path = {'hide':True}

    # is shell in chroot
    cl_chroot_status = {'hide':True}

    # locale (at example: ru_RU.UTF-8)
    os_locale_locale = {'hide':True}

    # full language (at example: ru_RU)
    os_locale_lang = {'hide':True}

    # short language (at example ru)
    os_locale_language = {'hide':True}

    # keyboard layout for X server
    os_locale_xkb = {'hide':True}

    # keyboard layout name for X server
    os_locale_xkbname = {'hide':True}

    # computer architecture (i686,x86_64)
    os_arch_machine = {'hide':True}

    # pass for templates join 1,2,3,4,5 and etc
    cl_pass_step = {'hide':True}

    # template file performed at now
    cl_pass_file = {'hide':True, 'mode':"w"}

    # root partition of filesystem
    os_root_dev = {'hide':True}

    # root type (ram, hdd, usb-hdd, livecd)
    os_root_type = {'hide':True}

    # full system name
    os_linux_name = {'hide':True}

    # postfix to system name (KDE GNOME and etc)
    os_linux_subname = {'hide':True}

    # system (desktop or server)
    os_linux_system = {'hide':True}

    # current timezone
    os_clock_timezone = {'hide':True}

    # motherboard model
    hr_board_model = {'hide':True}

    # motherboard vendor
    hr_board_vendor = {'hide':True}

    # cdrom device
    hr_cdrom_set = {}

    # processors count
    hr_cpu_num = {'hide':True}

    # virtual machine name (virtualbox, vmware, qemu)
    hr_virtual = {'hide':True}

    # system version
    os_linux_ver = {'hide':True}

    # system version
    os_linux_build = {'hide':True}

    # files number
    os_linux_filesnum = {'hide':True}

    # user login
    ur_login = {'hide':True}

    # user group name
    ur_group = {'hide':True}

    # user fullname
    ur_fullname = {'hide':True}

    # user home directory
    ur_home_path = {'hide':True}

    # path to directory relative which perform joining templates to system files
    # (sandbox)
    cl_root_path = {'hide':True, 'mode':"w"}

    # path to directory which contain other system
    cl_chroot_path = {'hide':True}

    # program action
    # install, uninstall, merge, domain, undomain, system, desktop
    cl_action = {'hide':True}

    # program state
    # specifies addition to cl_pass_action for needing
    cl_pass_state = {'hide':True}

    # User Jabber ID
    ur_jid = {'hide':True}

    # user email
    ur_mail = {'hide':True}

    # kernel uid get by uuid root device
    cl_kernel_uid = {'hide':True}

    # supported languages
    os_lang = {}

    # this variable work with template function belong(package_name)
    # if the variable is defined then will use only template, which 
    # has package_name in belong equal value of this variable or
    # hasn't belong function
    # (package_name == value of cl_belong_pkg)
    cl_belong_pkg = {'mode':'r', 'hide':True}

    # x11 resolution (1024x768 and etc)
    os_x11_resolution = {'mode':'w','hide':True}

    # vertical resolution for X server
    os_x11_height = {'mode':"r", 'hide':True}

    # horizontal resolution for X server
    os_x11_width = {'mode':"r", 'hide':True}

    # the nearest standard size of image to current screen resolution
    os_x11_standart = {'hide':True}

    # if computer is notebook, the this variable containt its vendor
    hr_laptop = {'hide':True}

    # if computer is notebook, model name
    hr_laptop_model = {'hide':True}

    # video verdor name
    hr_video = {'hide':True}

    # videocard name
    hr_video_name = {'hide':True}

    # Video driver used by xorg
    os_x11_video_drv = {'hide':True}

    # on/off composite mode
    os_x11_composite = {'hide':True}

    # current system is scratch
    os_scratch = {'hide':True}

    # programs have templates setup
    cl_merges = {'hide':True}

    # (on or off) autoupdate config from install program
    cl_autoupdate_set = {'hide':True}

    # The path to the module api, and additional parameters caluclate packages
    cl_api = {'hide':True}

    # base DN LDAP
    ld_base_dn = {'hide':True}

    # bind суффикс LDAP
    ld_bind_dn = {'hide':True}

    # bind name
    ld_bind_login = {'hide':True}

    # bind hash
    ld_bind_hash = {'hide':True}

    # bind password
    ld_bind_pw = {'hide':True}

    # hash crypto algoritm
    ld_encrypt = {'hide':True}

    # base name LDAP
    ld_base_root = {'hide':True}

    # admin DN
    ld_admin_dn = {'hide':True}

    # admin name LDAP
    ld_admin_login = {'hide':True}

    # admin hash
    ld_admin_hash = {'hide':True}

    # admin password
    ld_admin_pw = {'hide':True}

    # name from all services
    ld_services= {'hide':True}

    # DN from all services
    ld_services_dn =  {'hide':True}

    # CA certificate
    cl_ca_cert = {'hide':True}

    # CA key
    cl_ca_key = {'hide':True}

    # CA path
    cl_ca_path = {'hide':True}
