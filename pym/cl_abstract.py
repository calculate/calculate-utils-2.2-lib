# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod

class abs_api_min:
    __metaclass__ = ABCMeta

    @abstractmethod
    def is_setup(self):
        '''Is setup service (True/False)'''

    @abstractmethod
    def get_prioritet(self):
        '''Get prioritet 0-100
        users 50-70
        '''

    @abstractmethod
    def get_vars(self):
        '''Get Service vars
        '''


class abs_api_service(abs_api_min):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_service_name(self):
        '''Get name service'''

    @abstractmethod
    def is_start(self):
        '''Is run server (True/False)'''

    @abstractmethod
    def start(self):
        '''Start server'''

    @abstractmethod
    def restart(self):
        '''Restart server'''

    @abstractmethod
    def stop(self):
        '''Stop server'''

    @abstractmethod
    def is_runlevel(self):
        '''Is server in run level (True/False)'''

    @abstractmethod
    def add_runlevel(self):
        '''Add daemon to runlevel'''

    @abstractmethod
    def del_runlevel(self):
        '''Add daemon to runlevel'''

    @abstractmethod
    def del_vars_from_env(self):
        '''Delete template vars in env files
        '''

    @abstractmethod
    def get_pkg_name(self):
        '''Get package name
        '''

    @abstractmethod
    def apply_templates(self):
        '''Apply package templates
        '''