#-*- coding: utf-8 -*-

# Copyright 2008-2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys, os
from cl_template import iniParser
from cl_utils import convertStrListDict

class convertEnv:
    """Конвертирование переменных из /var/calculate/remote/calculate.env"""
    envPath = "/var/calculate/remote/calculate.env"
    convertData = {\
                "ur_organization":{"service":"mail","replace":"organization"},
                "ur_signature":{"service":"mail","replace":"signature"},
                "sr_mail_type":{"service":"mail","replace":"type"},
                "sr_mail_send_host":{"service":"mail","replace":"send_host"},
                "sr_mail_port":{"service":"mail","replace":"port"},
                "sr_mail_crypt":{"service":"mail","replace":"crypt"},
                "sr_mail_host":{"service":"mail","replace":"host"},
                "sr_mail_send_port":{"service":"mail","replace":"send_port"},
                "sr_mail_send_crypt":{"service":"mail","replace":"send_crypt"},
                "ld_unix_dn":{"service":"unix","replace":"dn"},
                "ld_bind_dn":{"service":"unix","replace":"bind_dn"},
                "ld_bind_pw":{"service":"unix","replace":"bind_pw"},
                "ld_services_dn":{"service":"ldap","replace":"services_dn"},
                "ld_base_dn":{"service":"ldap","replace":"base_dn"},
                "sr_jabber_crypt":{"service":"jabber","replace":"crypt"},
                "sr_jabber_host":{"service":"jabber","replace":"host"},
                "sr_jabber_port":{"service":"jabber","replace":"port"},
                "sr_samba_host":{"service":"samba","replace":"host"},
                "ld_samba_dn":{"service":"samba","replace":"dn"},
                "cl_remote_ftp":{"service":"ftp","replace":"host"},
                "sr_proxy_host":{"service":"proxy","replace":"host"},
                "sr_proxy_port":{"service":"proxy","replace":"port"}}
    convertVars = {}
    flagConverted = False

    def getVar(self, header, name):
        """Получаем сконвертированную переменную"""
        if not self.flagConverted:
            self.convertVars = self.convert()
            self.flagConverted = True
        if header in self.convertVars.keys() and\
            name in self.convertVars[header]:
            return self.convertVars[header][name]
        else:
            return ""

    def convert(self):
        """Конвертирование переменных сервера в словарь новых переменных"""
        parserObj = iniParser(self.envPath)
        clientVars = parserObj.getAreaVars("client")
        # convertVars = {"service":{"name":"value", ...}, ...}
        convertVars = {}
        for nameVar in clientVars.keys():
            if nameVar in self.convertData.keys():
                service = self.convertData[nameVar]["service"]
                nameVarService = self.convertData[nameVar]["replace"]
                value = convertStrListDict(clientVars[nameVar].encode("UTF-8"))
                if not service in convertVars.keys():
                    convertVars[service] = {}
                convertVars[service][nameVarService] = value
        return convertVars
