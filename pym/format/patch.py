#-*- coding: utf-8 -*-

# Copyright 2008-2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys, re
import xml.dom.minidom
from cl_utils import _error
# Перевод cообщений модуля
from cl_lang import lang
tr = lang()
tr.setLocalDomain('cl_lib')
tr.setLanguage(sys.modules[__name__])


class patch(_error):
    """Класс для замены, добавления, удаления, строк в файле"""
    # root нода
    rootNode = False
    # Документ
    doc = False
    # Текст шаблона
    text = ""

    def __init__(self, text):
        self.text = text
        # Создаем XML документ
        self.doc = self.textToXML()
        if self.doc:
            self.rootNode = self.doc.documentElement

    def textToXML(self):
        """Создание из текста XML документа
        Храним xml в своем формате
        """
        if not self.text.strip():
                self.text = ''
        text = '<?xml version="1.0" encoding="UTF-8"?>\n<patch>%s</patch>'\
                     %self.text
        try:
            self.doc = xml.dom.minidom.parseString(text)
        except:
            return False
        return self.doc

    def processingFile(self, textConfigFile):
        """Обработка конфигурационного файла"""
        if not self.doc:
            self.setError(_("Cannot convert the text template in XML"))
            return False
        retTextConfigFile = textConfigFile
        tags = ["reg", "text"]
        dataList = []
        tagsIndex = 0
        for node in  self.rootNode.childNodes:
            if node.nodeType==node.ELEMENT_NODE:
                if not node.tagName == tags[tagsIndex]:
                    self.setError(_("Incorrect template text"))
                    return False
                if tagsIndex == 1:
                    tagsIndex = 0
                else:
                    tagsIndex += 1
                # регулярное выражение
                if node.tagName == "reg":
                    if node.firstChild:
                        reText = node.firstChild.nodeValue
                        if reText is None:
                            textNode = node.toxml().encode("UTF8")
                            self.setError(\
                            _("Incorrect template text")+ ": \n" +\
                              "%s" %textNode)
                            return False
                    else:
                        self.setError(\
                            _("Incorrect text of template '<reg></reg>'"))
                        return False
                    if not reText.strip():
                        self.setError(\
                            _("Incorrect text of template '<reg>%s</reg>'")\
                               %reText)
                        return False
                    try:
                        regex = re.compile(reText)
                    except:
                        self.setError(\
                            _("Incorrect text of template '<reg>%s</reg>'")\
                              %reText)
                        return False
                elif node.tagName == "text" and regex:
                    if node.firstChild:
                        text = node.firstChild.nodeValue
                        if text is None:
                            textNode = node.toxml().encode("UTF8")
                            self.setError(\
                            _("Incorrect template text")+ ": \n" +\
                              "%s" %textNode)
                            return False
                    else:
                        text = ""
                    dataList.append((regex, text.encode('utf-8')))
                    regex = False
        for regex, text in dataList:
            # Замены в тексте конфигурационного файла
            retTextConfigFile = regex.sub(text, retTextConfigFile)
        return retTextConfigFile
