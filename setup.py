#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-lib

# Copyright 2008-2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys, os
from distutils.core import setup
from distutils.command.build_scripts import build_scripts
from distutils.command.install_scripts import install_scripts

__version__ = "2.2.30"
__app__ = "calculate-lib"

class cl_build_scripts(build_scripts):
    """Class for build scripts"""
    def run (self):
        scripts = ['./scripts/cl-update-config']
        backup_build_dir = self.build_dir
        backup_scripts = filter(lambda x: not x in scripts, self.scripts)
        self.scripts = scripts
        self.build_dir = self.build_dir + "-bin"
        build_scripts.run(self)
        if backup_scripts:
            self.scripts = backup_scripts
            self.build_dir = backup_build_dir
            build_scripts.run(self)


class cl_install_scripts(install_scripts):
    """Class for install scripts"""
    def run (self):
        backup_install_dir = self.install_dir
        backup_build_dir = self.build_dir
        cl_cmd_obj = self.distribution.get_command_obj("install")
        self.build_dir = self.build_dir + "-bin"
        self.install_dir = os.path.join(cl_cmd_obj.install_platlib, __app__,
                                        "bin")
        install_scripts.run(self)
        if os.path.exists(backup_build_dir):
            self.build_dir = backup_build_dir
            self.install_dir = backup_install_dir
            install_scripts.run(self)

setup(
    name = __app__,
    version = __version__,
    description = "The library for Calculate 2",
    author = "Mir Calculate Ltd.",
    author_email = "support@calculate.ru",
    url = "http://calculate-linux.org",
    license = "http://www.apache.org/licenses/LICENSE-2.0",
    package_dir = {'calculate-lib': "."},
    packages = ['calculate-lib.pym',
                'calculate-lib.pym.format',
                'calculate-lib.pym.server',
                'calculate-lib.pym.client',
                'calculate-lib.pym.update_config',
                'calculate-lib.pym.utils',
                'calculate-lib.pym.mod'],
    data_files = [("/etc/calculate", []),
                  ("/var/calculate/remote", []),
                  ("/var/log/calculate", [])],
    scripts=["./scripts/cl-update-config"],
    cmdclass={'build_scripts':cl_build_scripts,
              'install_scripts':cl_install_scripts},
)
